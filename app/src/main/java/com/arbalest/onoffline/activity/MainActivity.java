package com.arbalest.onoffline.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.arbalest.onoffline.R;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void openListActivity(int mode) {
        Intent intent = new Intent(this, ListActivity.class);
        intent.putExtra(ListActivity.INTENT_DATA_LIST_MODE, mode);
        startActivity(intent);
    }

    public void onDataListAutoClicked(View view) {
        openListActivity(ListActivity.AUTO);
    }

    public void onDataListOnlineClicked(View view) {
        openListActivity(ListActivity.ONLINE);
    }

    public void onDataListOfflineClicked(View view) {
        openListActivity(ListActivity.OFFLINE);
    }

    public void onSyncClicked(View view) {
        Intent intent = new Intent(this, SyncActivity.class);
        startActivity(intent);
    }
}
