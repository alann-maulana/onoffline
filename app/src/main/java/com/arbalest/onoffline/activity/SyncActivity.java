package com.arbalest.onoffline.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arbalest.onoffline.R;
import com.arbalest.onoffline.helper.ParseCloud;
import com.arbalest.onoffline.helper.SQLiteManager;
import com.arbalest.onoffline.helper.SQLiteModel;
import com.arbalest.onoffline.model.LogData;
import com.arbalest.onoffline.model.LogMode;
import com.arbalest.onoffline.model.Profile;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class SyncActivity extends AppCompatActivity {
    private static final String TAG = SyncActivity.class.getSimpleName();
    private TextView labelRowCount, labelLastUpdate, labelSyncStatus;
    private ProgressDialog loadingDownload, loadingSync;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        if (getSupportActionBar() != null) {
            // enable back button on action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        labelRowCount = (TextView) findViewById(R.id.sync_row_count);
        labelLastUpdate = (TextView) findViewById(R.id.sync_last_update);
        labelSyncStatus = (TextView) findViewById(R.id.sync_status);

        // creating instance of loading progress dialog
        loadingDownload = new ProgressDialog(this);
        loadingDownload.setMessage("Loading data...");
        loadingDownload.setIndeterminate(true);
        loadingDownload.setCancelable(true);
        loadingDownload.setCanceledOnTouchOutside(true);

        loadingSync = new ProgressDialog(this);
        loadingSync.setTitle("Sync Data");
        loadingSync.setMessage("Preparing for sync...");
        loadingSync.setIndeterminate(false);
        loadingSync.setMax(100);
        loadingSync.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        loadingSync.setCancelable(true);
        loadingDownload.setCanceledOnTouchOutside(true);

        getRowCountData();
        getLastUpdateFromSession();
        getSyncStatus();
    }

    public void onDownloadDataClicked(View view) {
        onClearDataClicked(view);
        downloadData();
    }

    public void onClearDataClicked(View view) {
        SQLiteManager.getInstance().deleteData(Profile.TABLE, null);
        labelRowCount.setText(getString(R.string.label_row_count, 0));
        putLastUpdateToSession();
        getLastUpdateFromSession();
    }

    public void onSyncDataClicked(View view) {
        if (SQLiteManager.getInstance().countTableRow(LogData.TABLE) <= 0) {
            Toast.makeText(this, "Sync data is empty!", Toast.LENGTH_LONG).show();
            return;
        }

        syncData();
    }

    private void getSyncStatus() {
        labelSyncStatus.setText(getString(R.string.label_status_sync,
                        SQLiteManager.getInstance().countTableRow(LogData.TABLE))
        );
    }

    private void getRowCountData() {
        labelRowCount.setText(getString(R.string.label_row_count,
                        SQLiteManager.getInstance().countTableRow(Profile.TABLE))
        );
    }

    private void putLastUpdateToSession() {
        PreferenceManager
                .getDefaultSharedPreferences(SyncActivity.this)
                .edit()
                .putLong(Profile.TABLE, new Date().getTime())
                .apply();
    }

    private void getLastUpdateFromSession() {
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        cal.setTime(new Date(PreferenceManager
                .getDefaultSharedPreferences(SyncActivity.this)
                .getLong(Profile.TABLE, System.currentTimeMillis())
        ));
        labelLastUpdate.setText(getString(R.string.label_last_update,
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                                .format(cal.getTime())
                )
        );
    }

    private void downloadData() {
        // create background thread task
        final AsyncTask<Void, String, Exception> task = new AsyncTask<Void, String, Exception>() {
            @Override
            protected void onPreExecute() {
                loadingDownload.setMessage("Downloading data...");
                loadingDownload.show();
            }

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    // iterate the object from cloud query result
                    for (ParseObject obj : ParseCloud.getDataAll()) {
                        // break iteration when task is cancelled
                        if (isCancelled()) break;
                        // ada profile object into list
                        SQLiteManager.getInstance().addDataSingle(Profile.TABLE, new Profile(obj));
                    }
                } catch (ParseException e) {
                    return e;
                }

                return null;
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(SyncActivity.this, "Loading data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    Toast.makeText(SyncActivity.this, "Error downloading data, cause : " + e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SyncActivity.this, "Downloading data succeed...", Toast.LENGTH_LONG).show();
                }

                putLastUpdateToSession();
                getLastUpdateFromSession();
                getRowCountData();

                // dismiss loading progress dialog
                loadingDownload.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loadingDownload.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (task.getStatus() != AsyncTask.Status.FINISHED) {
                    task.cancel(true);
                }
            }
        });

        // execute download data background task
        task.execute();
    }

    private void syncData() {
        // create background thread task
        final AsyncTask<Void, String, Exception> task = new AsyncTask<Void, String, Exception>() {
            @Override
            protected void onPreExecute() {
                loadingSync.setMessage("Preparing to sync data...");
                loadingSync.show();
            }

            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    List<SQLiteModel> sqLiteModels = SQLiteManager.getInstance().getDataAll(LogData.TABLE);
                    int length = sqLiteModels.size();
                    // iterate the object from local log data
                    for (int i=0; i<length; i++) {
                        LogData log = (LogData) sqLiteModels.get(i);

                        ParseObject profile;
                        if (log.getMode() == LogMode.INSERT || log.getMode() == LogMode.UPDATE) {
                            if (log.getMode() == LogMode.INSERT) {
                                publishProgress(LogMode.INSERT.name(), String.valueOf(i + 1), String.valueOf(length));
                                profile = new ParseObject(Profile.TABLE);
                            } else {
                                publishProgress(LogMode.UPDATE.name(), String.valueOf(i + 1), String.valueOf(length));
                                ParseQuery<ParseObject> query = ParseQuery.getQuery(Profile.TABLE);
                                query.whereEqualTo("id", log.getJson().optInt("id"));
                                profile = query.getFirst();
                                Log.d(TAG, "ObjectId to update : " + profile.getObjectId());
                            }
                            Log.d(TAG, "Data to update : " + log.getJson().toString());

                            Iterator<String> iterator = log.getJson().keys();
                            while (iterator.hasNext()) {
                                String key = iterator.next();
                                Object value = log.getJson().opt(key);

                                profile.put(key, value);
                            }
                            profile.save();
                        } else {
                            publishProgress(LogMode.DELETE.name(), String.valueOf(i + 1), String.valueOf(length));
                            ParseQuery<ParseObject> query = ParseQuery.getQuery(Profile.TABLE);
                            query.whereEqualTo("id", log.getJson().optInt("id"));
                            profile = query.getFirst();
                            Log.d(TAG, "ObjectId to delete : " + profile.getObjectId());
                            profile.delete();
                        }
                    }
                } catch (ParseException e) {
                    return e;
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                String message = values[0];
                int progress = Integer.parseInt(values[1]);
                int totalProgress = Integer.parseInt(values[2]);

                loadingSync.setMessage(String.format("[%s] Processing data %d of %d", message, progress, totalProgress));
                loadingSync.setProgress(progress * 100 / totalProgress);
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(SyncActivity.this, "Loading data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(Exception e) {
                if (e != null) {
                    Toast.makeText(SyncActivity.this, "Error syncing data, cause : " + e.getMessage(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SyncActivity.this, "Syncing data succeed...", Toast.LENGTH_LONG).show();
                }

                // clear log data
                SQLiteManager.getInstance().deleteData(LogData.TABLE, null);

                // update sync status
                getSyncStatus();

                // dismiss loading progress dialog
                loadingSync.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loadingSync.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (task.getStatus() != AsyncTask.Status.FINISHED) {
                    task.cancel(true);
                }
            }
        });

        // execute sync data background task
        task.execute();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
