package com.arbalest.onoffline.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.arbalest.onoffline.R;
import com.arbalest.onoffline.helper.NetworkUtil;
import com.arbalest.onoffline.helper.ParseCloud;
import com.arbalest.onoffline.helper.ProfileHolder;
import com.arbalest.onoffline.helper.SQLiteManager;
import com.arbalest.onoffline.helper.SQLiteModel;
import com.arbalest.onoffline.model.LogData;
import com.arbalest.onoffline.model.LogMode;
import com.arbalest.onoffline.model.Profile;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = ListActivity.class.getSimpleName();
    public static final String INTENT_DATA_LIST_MODE = "IntentDataListMode";
    public static final int AUTO = 0x00000;
    public static final int ONLINE = 0x00001;
    public static final int OFFLINE = 0x00002;

    private ListView listView;
    private SimpleAdapter adapter;
    private List<Map<String, String>> mapListData;
    private List<Profile> profileList;
    private ProgressDialog loading;
    private int dataListMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        dataListMode = getIntent().getIntExtra(INTENT_DATA_LIST_MODE, AUTO);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            // enable back button on action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            // set action bar title based on data list mode
            getSupportActionBar().setTitle(
                    dataListMode == AUTO ? "Data List Auto" :
                            dataListMode == ONLINE ? "Data List Online" :
                                    "Data List Offline"
            );
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                onAddDataClicked(view);

            }
        });

        listView = (ListView) findViewById(R.id.main_listview);
        listView.setOnItemClickListener(this);

        // creating instance of loading progress dialog
        loading = new ProgressDialog(this);
        loading.setMessage("Loading data...");
        loading.setIndeterminate(true);
        loading.setCancelable(true);
        loading.setCanceledOnTouchOutside(true);

        mapListData = new ArrayList<>();
        profileList = new ArrayList<>();

        // creating instance of adapter using SimpleAdapter
        adapter = new SimpleAdapter(this, mapListData,
                android.R.layout.simple_list_item_2,
                new String[]{"ID.NAME.COUNTRY", "EMAIL.IP_ADDRESS"},
                new int[]{android.R.id.text1, android.R.id.text2});

        // setting adapter to the listview
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();

        reloadData();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void reloadData() {
        @NetworkUtil.NetworkStatus
        final int status = NetworkUtil.getConnectivityStatus(ListActivity.this);

        // create background thread task
        final AsyncTask<Void, String, List<Profile>> task = new AsyncTask<Void, String, List<Profile>>() {
            @Override
            protected void onPreExecute() {
                loading.show();
            }

            @Override
            protected List<Profile> doInBackground(Void... params) {
                List<Profile> profileList = new ArrayList<>();

                if (status == NetworkUtil.TYPE_NOT_CONNECTED || dataListMode == OFFLINE) {
                    publishProgress("Loading offline data...");
                    for (SQLiteModel d : SQLiteManager.getInstance().getDataAll(Profile.TABLE)) {
                        // break iteration when task is cancelled
                        if (isCancelled()) break;
                        // ada profile object into list
                        profileList.add((Profile) d);
                    }
                } else {
                    publishProgress("Loading online data...");

                    try {
                        // iterate the object from cloud query result
                        for (ParseObject obj : ParseCloud.getDataAll()) {
                            // break iteration when task is cancelled
                            if (isCancelled()) break;
                            // ada profile object into list
                            profileList.add(new Profile(obj));
                        }
                    } catch (ParseException e) {
                        Log.e(TAG, "Error happen, cause : " + e.getMessage());
                    }
                }

                return profileList;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                // update loading message
                loading.setMessage(values[0]);
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(ListActivity.this, "Loading data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(List<Profile> profiles) {
                // clear all element on mapListData
                mapListData.clear();
                profileList.clear();

                // populate the list of profiles into mapListData
                for (Profile data : profiles) {
                    Map<String, String> map = new HashMap<>();
                    map.put("ID.NAME.COUNTRY", String.format("%d. %s %s (%s)",
                            data.getId(), data.getFirstName(), data.getLastName(), data.getCountry()));
                    map.put("EMAIL.IP_ADDRESS", String.format("Email : %s\nIP Address : %s",
                            data.getEmail(), data.getIpAddress()));
                    mapListData.add(map);
                    profileList.add(data);
                }

                // set the size of data on actionbar subtitle
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setSubtitle("Total : " + mapListData.size() + " profile(s)");
                }

                // refresh the view on adapter
                adapter.notifyDataSetChanged();

                // dismiss loading progress dialog
                loading.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (task.getStatus() != AsyncTask.Status.FINISHED) {
                    task.cancel(true);
                }
            }
        });

        // execute reload data background task
        task.execute();
    }

    private void onAddDataClicked(View view) {
        final ProfileHolder holder = new ProfileHolder(this, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Add Profile")
                .setView(holder.rootView)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton("Insert", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        insertData(new Profile(-1,
                                holder.firstName.getText().toString(),
                                holder.lastName.getText().toString(),
                                holder.email.getText().toString(),
                                holder.country.getText().toString(),
                                holder.ipAddress.getText().toString()));
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void onEditDataClicked(final int position, final Profile profile) {
        final ProfileHolder holder = new ProfileHolder(this, profile);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Edit Profile")
                .setView(holder.rootView)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateData(position, new Profile(
                                profile.getId(),
                                holder.firstName.getText().toString(),
                                holder.lastName.getText().toString(),
                                holder.email.getText().toString(),
                                holder.country.getText().toString(),
                                holder.ipAddress.getText().toString()));
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void onDeleteDataClicked(final int position, final Profile profile) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Confirmation")
                .setMessage(String.format("Delete '%s. %s %s' data?", profile.getId(), profile.getFirstName(), profile.getLastName()))
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData(position, profile);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    private void insertData(@NonNull final Profile data) {
        @NetworkUtil.NetworkStatus
        final int status = NetworkUtil.getConnectivityStatus(ListActivity.this);

        // create background thread task
        final AsyncTask<Void, String, Long> task = new AsyncTask<Void, String, Long>() {
            @Override
            protected void onPreExecute() {
                loading.show();
            }

            @Override
            protected Long doInBackground(Void... params) {
                long id = -1;

                if (status == NetworkUtil.TYPE_NOT_CONNECTED || dataListMode == OFFLINE) {
                    publishProgress("Saving offline data...");

                    // add single data profile into sqlite database
                    id = SQLiteManager.getInstance().addDataSingle(Profile.TABLE, data);

                    // set created id into data profile
                    data.setId((int) id);

                    // add log data insert
                    LogData logData = new LogData(LogMode.INSERT, data.toJSONObject());
                    SQLiteManager.getInstance().addDataSingle(LogData.TABLE, logData);
                } else {
                    publishProgress("Saving online data...");

                    try {
                        // save data into cloud
                        id = ParseCloud.insertData(data);

                        // set created id into data profile
                        data.setId((int) id);
                    } catch (ParseException e) {
                        Log.e(TAG, "Error happen, cause : " + e.getMessage());
                    }
                }

                return id;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                // update loading message
                loading.setMessage(values[0]);
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(ListActivity.this, "Saving data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(Long id) {
                // populate new data of profiles into mapListData
                Map<String, String> map = new HashMap<>();
                map.put("ID.NAME.COUNTRY", String.format("%d. %s %s (%s)",
                        data.getId(), data.getFirstName(), data.getLastName(), data.getCountry()));
                map.put("EMAIL.IP_ADDRESS", String.format("Email : %s\nIP Address : %s",
                        data.getEmail(), data.getIpAddress()));
                mapListData.add(map);
                profileList.add(data);

                // set the size of data on actionbar subtitle
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setSubtitle("Total : " + mapListData.size() + " profile(s)");
                }

                // refresh the view on adapter
                adapter.notifyDataSetChanged();

                // scroll list view to the end
                listView.smoothScrollToPosition(profileList.size() - 1);

                // dismiss loading progress dialog
                loading.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                task.cancel(true);
            }
        });

        // execute reload data background task
        task.execute();
    }

    private void updateData(final int position, @NonNull final Profile data) {
        @NetworkUtil.NetworkStatus
        final int status = NetworkUtil.getConnectivityStatus(ListActivity.this);

        // create background thread task
        final AsyncTask<Void, String, Long> task = new AsyncTask<Void, String, Long>() {
            @Override
            protected void onPreExecute() {
                loading.show();
            }

            @Override
            protected Long doInBackground(Void... params) {
                long rowsAffected = -1;

                if (status == NetworkUtil.TYPE_NOT_CONNECTED || dataListMode == OFFLINE) {
                    publishProgress("Updating offline data...");

                    // update single data profile into sqlite database
                    rowsAffected = SQLiteManager.getInstance().updateDataSingle(data);

                    // add log data update
                    LogData logData = new LogData(LogMode.UPDATE, data.toJSONObject());
                    SQLiteManager.getInstance().addDataSingle(LogData.TABLE, logData);
                } else {
                    publishProgress("Updating online data...");

                    try {
                        // update data into cloud
                        rowsAffected = ParseCloud.updateData(data);
                    } catch (ParseException e) {
                        Log.e(TAG, "Error happen, cause : " + e.getMessage());
                    }
                }

                return rowsAffected;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                // update loading message
                loading.setMessage(values[0]);
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(ListActivity.this, "Updating data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(Long id) {
                // populate updated data of profiles into mapListData
                Map<String, String> map = new HashMap<>();
                map.put("ID.NAME.COUNTRY", String.format("%d. %s %s (%s)",
                        data.getId(), data.getFirstName(), data.getLastName(), data.getCountry()));
                map.put("EMAIL.IP_ADDRESS", String.format("Email : %s\nIP Address : %s",
                        data.getEmail(), data.getIpAddress()));
                mapListData.set(position, map);
                profileList.set(position, data);

                // set the size of data on actionbar subtitle
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setSubtitle("Total : " + mapListData.size() + " profile(s)");
                }

                // refresh the view on adapter
                adapter.notifyDataSetChanged();

                // dismiss loading progress dialog
                loading.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                task.cancel(true);
            }
        });

        // execute reload data background task
        task.execute();
    }

    private void deleteData(final int position, @NonNull final Profile data) {
        @NetworkUtil.NetworkStatus
        final int status = NetworkUtil.getConnectivityStatus(ListActivity.this);

        // create background thread task
        final AsyncTask<Void, String, Long> task = new AsyncTask<Void, String, Long>() {
            @Override
            protected void onPreExecute() {
                loading.show();
            }

            @Override
            protected Long doInBackground(Void... params) {
                long rowsAffected = -1;

                if (status == NetworkUtil.TYPE_NOT_CONNECTED || dataListMode == OFFLINE) {
                    publishProgress("Deleting offline data...");

                    // add single data profile into sqlite database
                    rowsAffected = SQLiteManager.getInstance().deleteData(Profile.TABLE, data);

                    // add log data update
                    LogData logData = new LogData(LogMode.DELETE, data.toJSONObject());
                    SQLiteManager.getInstance().addDataSingle(LogData.TABLE, logData);
                } else {
                    publishProgress("Deleting online data...");

                    try {
                        // delete data in cloud
                        rowsAffected = ParseCloud.deleteData(data);
                    } catch (ParseException e) {
                        Log.e(TAG, "Error happen, cause : " + e.getMessage());
                    }
                }

                return rowsAffected;
            }

            @Override
            protected void onProgressUpdate(String... values) {
                // update loading message
                loading.setMessage(values[0]);
            }

            @Override
            protected void onCancelled() {
                Toast.makeText(ListActivity.this, "Deleting data cancelled...", Toast.LENGTH_LONG).show();
            }

            @Override
            protected void onPostExecute(Long id) {
                // populate updated data of profiles into mapListData
                mapListData.remove(position);
                profileList.remove(position);

                // set the size of data on actionbar subtitle
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setSubtitle("Total : " + mapListData.size() + " profile(s)");
                }

                // refresh the view on adapter
                adapter.notifyDataSetChanged();

                // dismiss loading progress dialog
                loading.dismiss();
            }
        };

        // set cancel listener on loading dialog
        loading.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                task.cancel(true);
            }
        });

        // execute reload data background task
        task.execute();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        final Profile profile = profileList.get(position);

        new AlertDialog.Builder(this)
                .setTitle(String.format("%s. %s %s", profile.getId(), profile.getFirstName(), profile.getLastName()))
                .setItems(new String[]{
                        "Edit", "Delete"
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                onEditDataClicked(position, profile);
                                break;
                            case 1:
                                onDeleteDataClicked(position, profile);
                                break;
                        }
                        dialog.dismiss();
                    }
                })
                .setCancelable(true)
                .show();
    }
}
