package com.arbalest.onoffline;

import android.app.Application;

import com.arbalest.onoffline.helper.SQLiteManager;
import com.parse.Parse;

/**
 * Created by Eyro on 12/14/15.
 */
public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        // initialize SQLite Manager instance
        SQLiteManager.initialize(this);

        // initialize Parse SDK instance
        Parse.initialize(this);
    }
}
