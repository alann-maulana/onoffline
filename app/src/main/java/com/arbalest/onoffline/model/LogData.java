package com.arbalest.onoffline.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.arbalest.onoffline.helper.SQLiteModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eyro on 12/18/15.
 */
public class LogData implements Parcelable, SQLiteModel {
    private int id;
    private LogMode mode;
    private JSONObject json;

    public LogData(LogMode mode, JSONObject json) {
        this.mode = mode;
        this.json = json;
    }

    public LogData(int id, LogMode mode, JSONObject json) {
        this(mode, json);
        this.id = id;
    }

    public LogData(Cursor cursor) throws JSONException {
        this(cursor.getInt(0), LogMode.valueOf(cursor.getString(1)), new JSONObject(cursor.getString(2)));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LogMode getMode() {
        return mode;
    }

    public void setMode(LogMode mode) {
        this.mode = mode;
    }

    public JSONObject getJson() {
        return json;
    }

    public void setJson(JSONObject json) {
        this.json = json;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mode == null ? -1 : this.mode.ordinal());
        dest.writeString(this.json.toString());
    }

    protected LogData(Parcel in) {
        int tmpMode = in.readInt();
        this.mode = tmpMode == -1 ? null : LogMode.values()[tmpMode];
        try {
            this.json = new JSONObject(in.readString());
        } catch (JSONException e) {
            this.json = null;
        }
    }

    public static final Creator<LogData> CREATOR = new Creator<LogData>() {
        public LogData createFromParcel(Parcel source) {
            return new LogData(source);
        }

        public LogData[] newArray(int size) {
            return new LogData[size];
        }
    };

    public static final String TABLE = "LogData";
    private static final String ID = "ID";
    private static final String MODE = "MODE";
    private static final String DATA = "DATA";
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE +
                "(" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    MODE + " TEXT," +
                    DATA + " TEXT" +
                ")";

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        //values.put(ID, this.id);
        values.put(MODE, this.mode.name());
        values.put(DATA, this.json.toString());

        return values;
    }

    @Override
    public String getWhereClause() {
        return "ID = ?";
    }

    @Override
    public String[] getWhereArgs() {
        return new String[] {String.valueOf(this.id)};
    }

    @Override
    public List<String> toListOfString() {
        List<String> list = new ArrayList<>();

        list.add(String.valueOf(this.id));
        list.add(this.mode.name());
        list.add(this.json.toString());

        return list;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();

        try {
            json.put("id", this.id);
            json.put("mode", this.mode);
            json.put("json", this.json);

            return json;
        } catch (JSONException e) {
            return null;
        }
    }
}
