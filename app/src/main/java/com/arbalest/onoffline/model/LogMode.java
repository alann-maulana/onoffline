package com.arbalest.onoffline.model;

/**
 * Created by Eyro on 12/18/15.
 */
public enum LogMode {
    INSERT,
    UPDATE,
    DELETE
}
