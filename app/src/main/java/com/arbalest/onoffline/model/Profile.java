package com.arbalest.onoffline.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.arbalest.onoffline.helper.SQLiteModel;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eyro on 12/14/15.
 */
public class Profile implements Parcelable, SQLiteModel {
    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String country;
    private String ipAddress;

    /**
     * Instantiates a new Profile.
     */
    public Profile() {
    }

    /**
     * Instantiates a new Profile.
     *
     * @param id        the id
     * @param firstName the first name
     * @param lastName  the last name
     * @param email     the email
     * @param country   the country
     * @param ipAddress the ip address
     */
    public Profile(int id, String firstName, String lastName, String email, String country, String ipAddress) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.country = country;
        this.ipAddress = ipAddress;
    }

    /**
     * Instantiates a new Profile from a {@code ParseObject}.
     *
     * @param obj the {@code ParseObject}
     */
    public Profile(ParseObject obj) {
        this(obj.getInt("id"), obj.getString("first_name"), obj.getString("last_name"),
                obj.getString("email"), obj.getString("country"), obj.getString("ip_address"));
    }

    /**
     * Instantiates a new Profile from a {@code Cursor}.
     *
     * @param cursor the {@code Cursor}
     */
    public Profile(Cursor cursor) {
        this(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5));
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets ip address.
     *
     * @return the ip address
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * Sets ip address.
     *
     * @param ipAddress the ip address
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.email);
        dest.writeString(this.country);
        dest.writeString(this.ipAddress);
    }

    /**
     * Instantiates a new Profile from a {@code Parcel}.
     *
     * @param in the {@code Parcel}
     */
    protected Profile(Parcel in) {
        this.id = in.readInt();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.email = in.readString();
        this.country = in.readString();
        this.ipAddress = in.readString();
    }

    /**
     * The constant CREATOR.
     */
    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel source) {
            return new Profile(source);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    /**
     * The constant TABLE.
     */
    public static final String TABLE = "Profile";
    private static final String ID = "ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String EMAIL = "EMAIL";
    private static final String COUNTRY = "COUNTRY";
    private static final String IP_ADDRESS = "IP_ADDRESS";
    /**
     * The constant CREATE_TABLE.
     */
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE +
                "(" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    FIRST_NAME + " TEXT," +
                    LAST_NAME + " TEXT," +
                    EMAIL + " TEXT," +
                    COUNTRY + " TEXT," +
                    IP_ADDRESS + " TEXT" +
                ")";

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        if (this.id > 0) {
            values.put(ID, this.id);
        }
        values.put(FIRST_NAME, this.firstName);
        values.put(LAST_NAME, this.lastName);
        values.put(EMAIL, this.email);
        values.put(COUNTRY, this.country);
        values.put(IP_ADDRESS, this.ipAddress);

        return values;
    }

    @Override
    public String getWhereClause() {
        return "ID = ?";
    }

    @Override
    public String[] getWhereArgs() {
        return new String[] {String.valueOf(this.id)};
    }

    @Override
    public List<String> toListOfString() {
        List<String> list = new ArrayList<>();

        list.add(String.valueOf(this.id));
        list.add(this.firstName);
        list.add(this.lastName);
        list.add(this.email);
        list.add(this.country);
        list.add(this.ipAddress);

        return list;
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject json = new JSONObject();

        try {
            json.put("id", this.id);
            json.put("first_name", this.firstName);
            json.put("last_name", this.lastName);
            json.put("email", this.email);
            json.put("country", this.country);
            json.put("ip_address", this.ipAddress);

        } catch (JSONException ignored) {}

        return json;
    }
}
