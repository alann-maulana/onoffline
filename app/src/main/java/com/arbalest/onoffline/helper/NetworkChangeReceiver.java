package com.arbalest.onoffline.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Class for receiving the broadcast intent when network status has been changed.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(final Context context, final Intent intent) {
 
        String status = NetworkUtil.getConnectivityStatusString(context);
 
        Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
    }
}