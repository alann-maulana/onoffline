package com.arbalest.onoffline.helper;

import android.content.ContentValues;

import org.json.JSONObject;

import java.util.List;

/**
 * Interface {@code SQLiteModel}.
 */
public interface SQLiteModel {
    /**
     * Convert object into content values.
     *
     * @return the content values of object
     */
    ContentValues toContentValues();

    /**
     * Getting the where clause.
     *
     * @return string of where clause
     */
    String getWhereClause();

    /**
     * Getting array string of where args.
     *
     * @return array string
     */
    String[] getWhereArgs();

    /**
     * Convert object into list of string.
     *
     * @return list of string
     */
    List<String> toListOfString();

    /**
     * Convert object into {@code JSONObject}.
     *
     * @return {@code JSONObject}
     */
    JSONObject toJSONObject();
}