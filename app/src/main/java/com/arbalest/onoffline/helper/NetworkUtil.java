package com.arbalest.onoffline.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Helper class for detecting network connectivity status.
 */
public class NetworkUtil {

    /**
     * The constant TYPE_WIFI.
     */
    public static final int TYPE_WIFI = 1;
    /**
     * The constant TYPE_MOBILE.
     */
    public static final int TYPE_MOBILE = 2;
    /**
     * The constant TYPE_NOT_CONNECTED.
     */
    public static final int TYPE_NOT_CONNECTED = 0;

    /**
     * The interface Network status.
     */
    @IntDef({TYPE_NOT_CONNECTED, TYPE_WIFI, TYPE_MOBILE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface NetworkStatus{}

    /**
     * Gets connectivity status.
     *
     * @param context the context activity where request connectivity status happen
     * @return the connectivity status
     */
    @NetworkStatus
    public static int getConnectivityStatus(@NonNull Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
 
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                return TYPE_WIFI;
            }
             
            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                return TYPE_MOBILE;
            }
        } 
        return TYPE_NOT_CONNECTED;
    }

    /**
     * Gets connectivity status string.
     *
     * @param context the context activity where request connectivity status happen
     * @return the connectivity status string
     */
    @Nullable
    public static String getConnectivityStatusString(@NonNull Context context) {
        @NetworkStatus
        int conn = NetworkUtil.getConnectivityStatus(context);

        String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }
}