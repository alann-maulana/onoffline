package com.arbalest.onoffline.helper;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.arbalest.onoffline.R;
import com.arbalest.onoffline.model.Profile;

/**
 * Created by Eyro on 12/18/15.
 */
public class ProfileHolder {
    public View rootView;
    public EditText profileId, firstName, lastName, email, country, ipAddress;

    public ProfileHolder(@NonNull Context context, @Nullable Profile data) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rootView = inflater.inflate(R.layout.dialog_profile, null);

        profileId = (EditText) rootView.findViewById(R.id.profileId);
        firstName = (EditText) rootView.findViewById(R.id.firstName);
        lastName = (EditText) rootView.findViewById(R.id.lastName);
        email = (EditText) rootView.findViewById(R.id.email);
        country = (EditText) rootView.findViewById(R.id.country);
        ipAddress = (EditText) rootView.findViewById(R.id.ipAddress);

        profileId.setEnabled(false);

        setData(data);
    }

    private void setData(@Nullable Profile data) {
        if (data == null) {
            //profileId.setHint(R.string.hint_profile_id_auto);
            return;
        }

        profileId.setText(String.valueOf(data.getId()));
        firstName.setText(data.getFirstName());
        lastName.setText(data.getLastName());
        email.setText(data.getEmail());
        country.setText(data.getCountry());
        ipAddress.setText(data.getIpAddress());
    }
}
