package com.arbalest.onoffline.helper;

import android.support.annotation.NonNull;

import com.arbalest.onoffline.model.Profile;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Eyro on 12/18/15.
 */
public class ParseCloud {
    public static List<ParseObject> getDataAll() throws ParseException {
        return ParseQuery.getQuery(Profile.TABLE).setLimit(150).find();
    }

    public static long insertData(@NonNull Profile data) throws ParseException {
        long id = getNextId();

        ParseObject profile = new ParseObject(Profile.TABLE);
        profile.put("id", id);
        profile.put("first_name", data.getFirstName());
        profile.put("last_name", data.getLastName());
        profile.put("country", data.getCountry());
        profile.put("email", data.getEmail());
        profile.put("ip_address", data.getIpAddress());
        profile.save();

        return id;
    }

    public static long updateData(@NonNull Profile data) throws ParseException {
        ParseObject profile = new ParseObject(Profile.TABLE);
        profile.put("first_name", data.getFirstName());
        profile.put("last_name", data.getLastName());
        profile.put("country", data.getCountry());
        profile.put("email", data.getEmail());
        profile.put("ip_address", data.getIpAddress());
        profile.save();

        return 1;
    }

    public static long deleteData(@NonNull Profile data) throws ParseException {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Profile.TABLE);
        query.whereEqualTo("id", data.getId());
        ParseObject profile = query.getFirst();
        profile.delete();

        return 1;
    }

    private static long getNextId() throws ParseException {
        ParseObject obj = ParseQuery.getQuery(Profile.TABLE)
                .addDescendingOrder("id")
                .getFirst();

        return obj.getNumber("id").intValue() + 1;
    }
}
