package com.arbalest.onoffline.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import android.util.Log;

import com.arbalest.onoffline.model.LogData;
import com.arbalest.onoffline.model.Profile;

import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class for managing SQLite database connection and transaction.
 */
public class SQLiteManager extends SQLiteOpenHelper {
    private static final String TAG = SQLiteManager.class.getSimpleName();

    // Database Info
    private static final String DATABASE_NAME = "OnOffLineDB";
    private static final int DATABASE_VERSION = 2;

    // Database instance
    private static volatile SQLiteManager mInstance;

    /**
     * The interface Table name.
     */
    @StringDef({Profile.TABLE, LogData.TABLE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface TableName{}

    /**
     * Initialize {@code SQLiteManager} instance.
     *
     * @param context the application context
     */
    public static void initialize(@NonNull Context context) {
        if (mInstance == null) {
            synchronized (SQLiteManager.class) {
                if (mInstance == null) {
                    Context applicationContext = context.getApplicationContext();
                    File externalFilesDir = context.getExternalFilesDir(null);
                    String absolutePath = externalFilesDir == null
                            ? "" : externalFilesDir.getAbsolutePath();
                    String dbPath = (TextUtils.isEmpty(absolutePath)
                            ? "" : absolutePath + File.separator) + DATABASE_NAME;
                            mInstance = new SQLiteManager(applicationContext, dbPath);
                    Log.d(TAG, "SQLiteManager instance initialized...");
                }
            }
        }
    }

    /**
     * Get {@code SQLiteManager} instance.
     *
     * @return {@code SQLiteManager} instance
     */
    public static synchronized SQLiteManager getInstance(){
        if (mInstance == null) {
            throw new RuntimeException("Call SQLiteManager#initialize(Context) first on Application#onCreate!");
        }
        return mInstance;
    }

    /**
     * Private constructor of {@code SQLiteManager}
     *
     * @param context the application context
     * @param dbPath the database absolute path
     */
    private SQLiteManager(Context context, String dbPath){
        super(context, dbPath, null, DATABASE_VERSION);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create table sync mapping
        db.execSQL(Profile.CREATE_TABLE);
        db.execSQL(LogData.CREATE_TABLE);
        Log.d(TAG, "DB created...");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Profile.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + LogData.TABLE);
            db.delete("SQLITE_SEQUENCE", "name = ?", new String[]{Profile.TABLE});
            db.delete("SQLITE_SEQUENCE", "name = ?", new String[]{LogData.TABLE});
            onCreate(db);
            Log.d(TAG, "DB upgraded...");
        }
    }

    /**
     * Gets table row count.
     *
     * @param tableName the table name
     * @return the table row count
     */
    public int countTableRow(@TableName String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        int rowCount = 0;

        try {
            String countQuery = "SELECT COUNT(*) FROM " + tableName;
            Cursor cursor = db.rawQuery(countQuery, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    rowCount = cursor.getInt(0);
                }

                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to count row of table " + tableName + "! Message: " + e.getMessage());
        }

        return rowCount;
    }

    public int getProfileAutoIncrement() {
        SQLiteDatabase db = getReadableDatabase();
        int autoIncrement = 0;

        try {
            String countQuery = "SELECT seq FROM SQLITE_SEQUENCE WHERE name = '" + Profile.TABLE + "'";
            Cursor cursor = db.rawQuery(countQuery, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    autoIncrement = cursor.getInt(0);
                }

                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to get sequence of table Profile! Message: " + e.getMessage());
        }

        return autoIncrement;
    }

    /**
     * Getting list of all {@code SQLiteModel} data.
     *
     * @param tableName string of table name
     * @return list of all {@code SQLiteModel} data
     */
    public List<SQLiteModel> getDataAll(@TableName String tableName) {
        SQLiteDatabase db = getReadableDatabase();
        List<SQLiteModel> results = new ArrayList<>();

        try {
            String tableInfo = "SELECT * FROM " + tableName;
            Cursor cursor = db.rawQuery(tableInfo, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        if (tableName.equals(Profile.TABLE)) {
                            results.add(new Profile(cursor));
                        } else {
                            results.add(new LogData(cursor));
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to get all row of table " + tableName + "! Message: " + e.getMessage());
        }

        return results;
    }

    /**
     * Add single data into table.
     *
     * @param data object data that implements {@code SQLiteModel}
     * @return id of row created, otherwise return -1
     */
    public long addDataSingle(@TableName String tableName, @NonNull SQLiteModel data) {
        long id = -1;
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            id = db.insertOrThrow(tableName, null, data.toContentValues());
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to add row of "+ tableName +"! Message: " + e.getMessage());
        } finally {
            db.endTransaction();
        }

        return id;
    }

    /**
     * Update single data.
     *
     * @param data object data that implements {@code SQLiteModel}
     * @return the number of rows affected
     */
    public int updateDataSingle(@NonNull SQLiteModel data) {
        int rowsAffected = 0;
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            rowsAffected = db.update(Profile.TABLE, data.toContentValues(), data.getWhereClause(), data.getWhereArgs());
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to add row of "+ Profile.TABLE +"! Message: " + e.getMessage());
        } finally {
            db.endTransaction();
        }

        return rowsAffected;
    }

    /**
     * Deleting all data from {@code Profile} table.
     *
     * @param tableName string of table name
     * @param data object data that implements {@code SQLiteModel}
     * @return total row(s) deleted
     */
    public int deleteData(@TableName String tableName, @Nullable SQLiteModel data) {
        SQLiteDatabase db = getWritableDatabase();
        int rowDeleted = 0;

        try {
            rowDeleted = db.delete(tableName,
                    data == null ? "1" : data.getWhereClause(),
                    data == null ? null : data.getWhereArgs());

            if (data == null) {
                // reset auto increment table
                db.delete("SQLITE_SEQUENCE", "name = ?", new String[]{ tableName });
            }

            Log.d(TAG, tableName + " deleted : " + rowDeleted);
        } catch (Exception e) {
            Log.e(TAG, "Error while trying to empty table "+ tableName +"! Message: " + e.getMessage());
        }

        return rowDeleted;
    }
}